# FROM base/archlinux
# FROM techne/arch-rpi-base
FROM xywsp/byodb_archl:devel
MAINTAINER R. Schaupp <xywsp@live.de>

# based also on works of pravka/arch-base and expanding therefrom

# base
######

# update mirror list for arch-arm server
# RUN echo 'Server = http://mirror.archlinuxarm.org/$arch/$repo' > /etc/pacman.d/mirrorlist

# set environment variables
ENV HOME /root
ENV LANG de_DE.UTF-8

# set locale
RUN echo de_DE.UTF-8 UTF-8 > /etc/locale.gen
RUN locale-gen
RUN echo LANG="de_DE.UTF-8" > /etc/locale.conf

# perform system update (must ignore package "filesystem")
RUN pacman -Syu --ignore filesystem --noconfirm

# add in pre-req from official repo
RUN pacman -S supervisor --noconfirm

# add supervisor configuration file
ADD supervisor.conf /etc/supervisor.conf

# home
######

# create user home directory
# RUN mkdir -p /home/xywsp

# set permissions
# RUN chown -R xywsp:xywsp /home/xywsp
# RUN chmod -R 775 /home/xywsp

# cleanup
#########

# remove unwanted system files
RUN rm -rf /archlinux/usr/share/locale
RUN rm -rf /archlinux/usr/share/man

# install applications
######################

# update package databases for arch
RUN pacman -Sy --noconfirm

# install webstack applications
RUN pacman -S nginx gd php-fpm php-apcu php-pear php-sqlite php-gd --ignore shadow systemd --noconfirm

# map /config to host defined config path (used to store configuration from app)
VOLUME /config

# map /data to host defined data path (used to store data from app [logs, etc])
VOLUME /data

# expose port for http
EXPOSE 80

# expose port for https
EXPOSE 443

# copy prerun bash shell script (checks for existence of nginx config)
ADD prerun.sh /etc/supervisor/conf.d/prerun.sh

# make prerun bash shell script executable
RUN chmod +x /etc/supervisor/conf.d/prerun.sh

# set permissions
#################

# change owner
RUN chown -R http:http /usr/sbin/nginx

# set permissions
RUN chmod -R 775 /usr/sbin/nginx

# add conf file
###############

ADD nginx.conf /etc/supervisor/conf.d/nginx.conf

# cleanup
#########

# completely empty pacman cache folder
RUN pacman -Scc --noconfirm

# remove temporary files
RUN rm -rf /tmp/*
RUN rm -rf /root/*

# run supervisor
CMD ["supervisord", "-c", "/etc/supervisor.conf", "-n"]
