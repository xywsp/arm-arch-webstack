#!/bin/bash

# check if nginx.conf exists, if not copy sample config
if [ -f /config/nginx.conf ]; then

    echo "nginx.conf exists"
    
else

    # copy to /config and set owner
    cp /etc/nginx/nginx /config
    chown http:http /config/nginx.conf
    
fi
